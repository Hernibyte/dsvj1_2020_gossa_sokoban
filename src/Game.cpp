#include"Game.h"

Game::Game() {
	window = new Window("SOKOBAN", 1280, 1000, 30);
	loop = new Loop();
	data = new DataManager();
}
Game::~Game() {
	if (window != NULL) delete window;
	if (loop != NULL) delete loop;
	if (data != NULL) delete data;
}
void Game::StartGame() {
	if (window != NULL) {
		window->StartWindow();
		window->setWindowFps();
		//----
		InitTextures();
		InitAudioDevice();
		SetSounds();
		//----
		if (loop != NULL) loop->StartLoop(data);
		//----
		DeleteTextures();
		DeleteSounds();
		CloseAudioDevice();
		//----
		window->WindowClose();
	}
}
void Game::InitTextures() {
	data->SetPlayButtonTr("../res/assets/PlayButton.png");
	data->SetCloseButtonTr("../res/assets/CloseButton.png");
	data->SetPauseButtonTr("../res/assets/PauseButton.png");
	data->SetBricksTopTr("../res/assets/BricksTop.png");
	data->SetBricksDownTr("../res/assets/BricksDown.png");
	data->SetFloorTr("../res/assets/Floor.png");
	data->SetPressurePlateTr("../res/assets/PressurePlate.png");
	data->SetBoxTr("../res/assets/Box.png");
	data->SetPlayerTr("../res/assets/Player.png");
}
void Game::DeleteTextures() {
	data->DeletePlayButtonTr();
	data->DeleteCloseButtonTr();
	data->DeletePauseButtonTr();
	data->DeleteBricksTopTr();
	data->DeleteBricksDownTr();
	data->DeleteFloorTr();
	data->DeletePressurePlateTr();
	data->DeleteBoxTr();
	data->DeletePlayerTr();
}

void Game::SetSounds() {
	data->SetMenuSound("../res/assets/MenuSoundTrack.mp3");
	data->SetGameSound("../res/assets/Game.mp3");
	data->SetPasoSound("../res/assets/Paso.mp3");
}

void Game::DeleteSounds() {
	data->DeleteMenuSound();
	data->DeleteGameSound();
	data->DeletePasoSound();
}