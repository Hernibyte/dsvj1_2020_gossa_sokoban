#include"Draw.h"

Draw::Draw() {
	menu = new Menu();
	gameplay = new Gameplay();
}
Draw::~Draw() {
	if (menu != NULL) delete menu;
	if (gameplay != NULL) delete gameplay;
}

//---

void Draw::process(DataManager* data) {
	BeginDrawingGame();
	ClearWindowBackground();
	//---
	switch (data->GetGameInstance()) {
	case 0:
		menu->DrawSwitcher(data);
		break;
	case 1:
		gameplay->DrawSwitcher(data);
		break;
	}
	//---
	EndDrawingGame();
}
void Draw::BeginDrawingGame() {
	BeginDrawing();
}
void Draw::ClearWindowBackground() {
	ClearBackground(BLACK);
}
void Draw::EndDrawingGame() {
	EndDrawing();
}