#ifndef MENU_H
#define MENU_H
#include"Input.h"

class Menu {
private:
	Input* input;
public:
	Menu();
	~Menu();
	//---
	void LogicSwitcher(DataManager* data);
	void PrincipalMenuInstance(DataManager* data);
	void OptionMenuInstance(DataManager* data);
	void SoundMenuInstance(DataManager* data);
	//---
	void DrawSwitcher(DataManager* data);
	void DrawPrincMenuInst(DataManager* data);
	void DrawOptMenuInst(DataManager* data);
	void DrawSoundMenuInst(DataManager* data);
};

#endif // !MENU_H