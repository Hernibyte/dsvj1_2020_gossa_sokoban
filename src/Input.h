#ifndef INPUT_H
#define INPUT_H
#include"Global_Includes.h"

class Input {
public:
	bool MouseClickPressed(int type);
	bool MouseClickDown(int type);
	bool MouseClickUp(int type);
	bool MouseClickReleased(int type);
	//---
	bool ButtonPressed(int type);
	bool ButtonDown(int type);
	bool ButtonUp(int type);
	bool ButtonReleased(int type);
};

#endif // !INPUT_H