#ifndef DRAW_H
#define DRAW_H
#include"Process.h"
#include"Menu.h"
#include"Gameplay.h"

class Draw : public Process {
private:
	Menu* menu;
	Gameplay* gameplay;
public:
	Draw();
	~Draw();
	//---
	void process(DataManager* data);
	void BeginDrawingGame();
	void ClearWindowBackground();
	void EndDrawingGame();
};

#endif // !DRAW_H