#ifndef PLAYER_H
#define PLAYER_H
#include"Global_Includes.h"
#include"DataManager.h"
#include"Input.h"

class Player {
private:
	Input* input;
public:
	Player();
	~Player();
	//---
	void Move(DataManager* data, int _mapTamX, int _mapTamY);
};

#endif // !PLAYER_H