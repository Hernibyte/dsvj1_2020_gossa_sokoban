#include"Loop.h"

Loop::Loop() {
	for (int i = 0; i < 1; i++) {
		process[i] = new Logic();
	}
	for (int i = 1; i < 2; i++) {
		process[i] = new Draw();
	}
}
Loop::~Loop() {
	for (int i = 0; i < TAM; i++) {
		if (process[i] != NULL) delete process[i];
	}
}
void Loop::StartLoop(DataManager* data) {
	while (!WindowShouldClose() && !data->GetCloseGame()) {
		for (int i = 0; i < TAM; i++) {
			if (process[i] != NULL) process[i]->process(data);
		}
	}
}