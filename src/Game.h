#ifndef GAME_H
#define GAME_H
#include"Window.h"
#include"Loop.h"

class Game {
private:
	Window* window;
	Loop* loop;
	DataManager* data;
public:
	Game();
	~Game();
	void StartGame();
	void InitTextures();
	void DeleteTextures();
	void SetSounds();
	void DeleteSounds();
};

#endif // !GAME_H