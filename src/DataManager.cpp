#include"DataManager.h"

DataManager::DataManager() {
	SetCloseGame(false);
	SetDelta(0.0f);
	//---
	SetGameInstance(0);
	SetInMenuInstance(0);
	SetGamelvl(0);
	//---
	SetPlayButton(540, 400, 255, 255);
	//---
	SetCloseButton(1150, 20, 128, 128);
	//---
	SetOptionButton(40, 950, 30);
	//---
	SetPauseButton(1150, 20, 128, 128);
	//---
	SetRestartButton(1000, 300, 180, 180);
	//---
	SetLeaveButton(1000, 500, 180, 180);
	//---
	SetLoadBar(100, POSBARY, 0, 5);
	//---
	SetWinlvl(false);
}
DataManager::~DataManager() {

}

//---

void DataManager::SetCloseGame(bool _closeGame) {
	closeGame = _closeGame;
}
bool DataManager::GetCloseGame() {
	return closeGame;
}

//---

void DataManager::SetDelta(float _delta) {
	delta = _delta;
}
void DataManager::PlusDelta(float _delta) {
	delta += _delta;
}
float DataManager::GetDelta() {
	return delta;
}

//---

void DataManager::SwitchPause() {
	pauseGame = !pauseGame;
}
float DataManager::GetPause() {
	return pauseGame;
}

//--- Textures

void DataManager::SetPlayButtonTr(const char* filePath) {
	PlayButtonTr = LoadTexture(filePath);
}
Texture2D DataManager::GetPlayButtonTr() {
	return PlayButtonTr;
}
void DataManager::DeletePlayButtonTr() {
	UnloadTexture(PlayButtonTr);
}

void DataManager::SetCloseButtonTr(const char* filePath) {
	closeButtonTr = LoadTexture(filePath);
}
Texture2D DataManager::GetCloseButtonTr() {
	return closeButtonTr;
}
void DataManager::DeleteCloseButtonTr() {
	UnloadTexture(closeButtonTr);
}

void DataManager::SetBricksTopTr(const char* filePath) {
	brickTopTr = LoadTexture(filePath);
}
Texture2D DataManager::GetBricksTopTr() {
	return brickTopTr;
}
void DataManager::DeleteBricksTopTr() {
	UnloadTexture(brickTopTr);
}

void DataManager::SetBricksDownTr(const char* filePath) {
	brickDownTr = LoadTexture(filePath);
}
Texture2D DataManager::GetBricksDownTr() {
	return brickDownTr;
}
void DataManager::DeleteBricksDownTr() {
	UnloadTexture(brickDownTr);
}

void DataManager::SetFloorTr(const char* filePath) {
	floorTr = LoadTexture(filePath);
}
Texture2D DataManager::GetFlorTr() {
	return floorTr;
}
void DataManager::DeleteFloorTr() {
	UnloadTexture(floorTr);
}

void DataManager::SetPressurePlateTr(const char* filePath) {
	pressurePlateTr = LoadTexture(filePath);
}
Texture2D DataManager::GetPressurePlateTr() {
	return pressurePlateTr;
}
void DataManager::DeletePressurePlateTr() {
	UnloadTexture(pressurePlateTr);
}

void DataManager::SetBoxTr(const char* filePath) {
	boxTr = LoadTexture(filePath);
}
Texture2D DataManager::GetBoxTr() {
	return boxTr;
}
void DataManager::DeleteBoxTr() {
	UnloadTexture(boxTr);
}

void DataManager::SetPlayerTr(const char* filePath) {
	playerTr = LoadTexture(filePath);
}
Texture2D DataManager::GetPlayerTr() {
	return playerTr;
}
void DataManager::DeletePlayerTr() {
	UnloadTexture(playerTr);
}
//--- Sounds
void DataManager::SetMenuSound(const char* fileName) {
	MenuSoundTrack = LoadSound(fileName);
}
Sound DataManager::GetMenuSound() {
	return MenuSoundTrack;
}
void DataManager::DeleteMenuSound() {
	UnloadSound(MenuSoundTrack);
}
void DataManager::SwitchMenuSoundActivate() {
	MenuSoundActivate = !MenuSoundActivate;
}
bool DataManager::GetMenuSoundActivate() {
	return MenuSoundActivate;
}

void DataManager::SetGameSound(const char* fileName) {
	GameSound = LoadSound(fileName);
}
Sound DataManager::GetGameSound() {
	return GameSound;
}
void DataManager::DeleteGameSound() {
	UnloadSound(GameSound);
}
void DataManager::SetGameActivate(bool _gameActivate) {
	GameSoundActivate = _gameActivate;
}
bool DataManager::GetGameActivate() {
	return GameSoundActivate;
}

void DataManager::SetPasoSound(const char* fileName) {
	PasoSound = LoadSound(fileName);
}
Sound DataManager::GetPasoSound() {
	return PasoSound;
}
void DataManager::DeletePasoSound() {
	UnloadSound(PasoSound);
}

//---


void DataManager::SetGameInstance(int _gameInstance) {
	gameInstance = _gameInstance;
}
int DataManager::GetGameInstance() {
	return gameInstance;
}
void DataManager::SetInMenuInstance(int _inMenuInstance) {
	inMenuIntance = _inMenuInstance;
}
int DataManager::GetInMenuInstance() {
	return inMenuIntance;
}
void DataManager::SetInGameInstance(int _inGameInstace) {
	inGameInstance = _inGameInstace;
}
int DataManager::GetInGameInstance() {
	return inGameInstance;
}
void DataManager::SetGamelvl(int _gamelvl) {
	gamelvl = _gamelvl;
}
int DataManager::GetGamelvl() {
	return gamelvl;
}

//---

void DataManager::SetPlayButton(float _x, float _y, float _width, float _height) {
	playButton.x = _x;
	playButton.y = _y;
	playButton.width = _width;
	playButton.height = _height;

	playButtonPos.x = _x;
	playButtonPos.y = _y;
}
Rectangle DataManager::GetPlayButtonPos() {
	return playButton;
}
Vector2 DataManager::GetPlayButtonPosition() {
	return playButtonPos;
}
void DataManager::SetCheckPlayButton(bool _checkPlayButton) {
	checkPlayButton = _checkPlayButton;
}
bool DataManager::GetCheckPlayButton() {
	return checkPlayButton;
}

//---

void DataManager::SetCloseButton(float _x, float _y, float _width, float _height) {
	closeButton.x = _x;
	closeButton.y = _y;
	closeButton.width = _width;
	closeButton.height = _height;

	closeButtonPos.x = _x;
	closeButtonPos.y = _y;
}
Rectangle DataManager::GetCloseButton() {
	return closeButton;
}
Vector2 DataManager::GetCloseButtonPos() {
	return closeButtonPos;
}
void DataManager::SetCheckCloseButton(bool _checkCloseButton) {
	checkCloseButton = _checkCloseButton;
}
bool DataManager::GetCheckCloseButton() {
	return checkCloseButton;
}

//---

void DataManager::SetOptionButton(int _optionButtonCenterX, int _optionButtonCenterY, float _optionButtonRadius) {
	optionButtonCenter.x = _optionButtonCenterX;
	optionButtonCenter.y = _optionButtonCenterY;
	optionButtonRadius = _optionButtonRadius;
}
Vector2 DataManager::GetOptionButtonCenter() {
	return optionButtonCenter;
}
float DataManager::GetOptionButtonRadius() {
	return optionButtonRadius;
}
void DataManager::SetCheckOptionButton(bool _checkOptionButton) {
	checkOptionButton = _checkOptionButton;
}
bool DataManager::GetCheckOptionButton() {
	return checkOptionButton;
}

//---

void DataManager::SetPauseButton(float _x, float _y, float _width, float _height) {
	pauseButton.x = _x; 
	pauseButton.y = _y;
	pauseButton.width = _width;
	pauseButton.height = _height;

	pauseButtonPos.x = _x;
	pauseButtonPos.y = _y;
}
Rectangle DataManager::GetPauseButton() {
	return pauseButton;
}
Vector2 DataManager::GetPauseButtonPos() {
	return pauseButtonPos;
}
void DataManager::SetCheckPauseButton(bool _checkPauseButton) {
	checkPauseButton = _checkPauseButton;
}
bool DataManager::GetCheckPauseButton() {
	return checkPauseButton;
}
void DataManager::SetPauseButtonTr(const char* filePath) {
	pauseButtonTr = LoadTexture(filePath);
}
Texture2D DataManager::GetPauseButtonTr() {
	return pauseButtonTr;
}
void DataManager::DeletePauseButtonTr() {
	UnloadTexture(pauseButtonTr);
}

//---
void DataManager::SetRestartButton(float _x, float _y, float _width, float _height) {
	restartButton.x = _x;
	restartButton.y = _y;
	restartButton.width = _width;
	restartButton.height = _height;

	restartButtonPos.x = _x;
	restartButtonPos.y = _y;
}
Rectangle DataManager::GetRestartButton() {
	return restartButton;
}
Vector2 DataManager::GetRestartButtonPos() {
	return restartButtonPos;
}
void DataManager::SetCheckRestartButton(bool _checkRestartButton) {
	checkRestartButton = _checkRestartButton;
}
bool DataManager::GetCheckRestartButton() {
	return checkRestartButton;
}

//---

void DataManager::SetLeaveButton(float _x, float _y, float _width, float _height) {
	leaveButton.x = _x;
	leaveButton.y = _y;
	leaveButton.width = _width;
	leaveButton.height = _height;

	leaveButtonPos.x = _x;
	leaveButtonPos.y = _y;
}
Rectangle DataManager::GetLeaveButton() {
	return leaveButton;
}
Vector2 DataManager::GetLeaveButtonPos() {
	return leaveButtonPos;
}
void DataManager::SetCheckLeaveButton(bool _checkLeaveButton) {
	checkLeaveButton = _checkLeaveButton;
}
bool DataManager::GetCheckLeaveButton() {
	return checkLeaveButton;
}

//---

void DataManager::SetLoadBar(float _x, float _y, float _width, float _height) {
	loadBar = { _x, _y, _width, _height };
}
Rectangle DataManager::GetLoadBar() {
	return loadBar;
}

//---

void DataManager::SetWinlvl(bool _checkWinlvl) {
	checkWinlvl = _checkWinlvl;
}
void DataManager::SwitchWinlvl() {
	checkWinlvl = !checkWinlvl;
}
bool DataManager::GetWinlvl() {
	return checkWinlvl;
}
void DataManager::CheckWinlvl(int _mapTamX, int _mapTamY, int _CantButtons) {
	int q = 0;
	for (int i = 0; i < _mapTamX; i++) {
		for (int j = 0; j < _mapTamY; j++) {
			if (GetTypeInlvl3(i, j).x == PLACE_BUTTON && GetTypeInlvl3(i, j).y == PLACE_BOX) {
				q++;
			}
		}
	}
	if (q >= _CantButtons) {
		SetWinlvl(true);
	}
}

//---
void DataManager::Generatelvl1() {
	float initplaceX = INITPLACEX;
	float initplaceY = INITPLACEY;
	float separationplace = SEPARATIONPLACE;

	for (int i = 0; i < 8; i++) {
		lvl3Pos[i][0] = { initplaceX, initplaceY };
		lvl3Type[i][0] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 3; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 5; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 8; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 2; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 4; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 5; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 8; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 3; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 6; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 7; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 3; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 4; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 5; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 8; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 3; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 5; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 8; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 3; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 5; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 8; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 3; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 4; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_EMPLY, PLACE_PLAYER };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 5; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 8; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 8; i++) {
		lvl3Pos[i][8] = { initplaceX, initplaceY };
		lvl3Type[i][8] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
}
void DataManager::Generatelvl2() {
	float initplaceX = INITPLACEX;
	float initplaceY = INITPLACEY;
	float separationplace = SEPARATIONPLACE;

	for (int i = 0; i < 8; i++) {
		lvl3Pos[i][0] = { initplaceX, initplaceY };
		lvl3Type[i][0] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 2; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_EMPLY, PLACE_PLAYER };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 6; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 7; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 3; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 4; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 5; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 8; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 3; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 4; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 5; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 8; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 2; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 3; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 4; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 5; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 8; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 3; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 5; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 8; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 2; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 3; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 4; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 5; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 6; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 8; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 3; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 6; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 8; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 8; i++) {
		lvl3Pos[i][8] = { initplaceX, initplaceY };
		lvl3Type[i][8] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
}
void DataManager::Generatelvl4() {
	float initplaceX = INITPLACEX;
	float initplaceY = INITPLACEY;
	float separationplace = SEPARATIONPLACE;

	for (int i = 0; i < 8; i++) {
		lvl3Pos[i][0] = { initplaceX, INITPLACEY };
		lvl3Type[i][0] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 3; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 6; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 8; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 2; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 3; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_EMPLY, PLACE_PLAYER };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 4; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 6; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 8; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 3; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 4; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 5; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 6; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 8; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 2; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 4; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 5; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 6; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 8; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 2; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 3; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 4; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 5; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 6; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 8; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 3; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 4; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 6; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 7; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 4; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 5; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 7; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 8; i++) {
		lvl3Pos[i][8] = { initplaceX, initplaceY };
		lvl3Type[i][8] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
}
void DataManager::Generatelvl5() {
	float initplaceX = INITPLACEX;
	float initplaceY = INITPLACEY;
	float separationplace = SEPARATIONPLACE;

	for (int i = 0; i < 8; i++) {
		lvl3Pos[i][0] = { initplaceX, INITPLACEY };
		lvl3Type[i][0] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 2; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 3; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 6; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 7; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 4; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 5; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 7; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 2; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 3; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 4; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 5; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 7; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 2; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 3; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 5; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 7; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 3; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 4; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 6; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 7; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_EMPLY, PLACE_PLAYER};
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 3; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 5; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 8; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BRICK, PLACE_EMPLY};
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 2; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 5; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 6; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 8; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 8; i++) {
		lvl3Pos[i][8] = { initplaceX, initplaceY };
		lvl3Type[i][8] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
}
void DataManager::Generatelvl3() {
	float initplaceX = INITPLACEX;
	float initplaceY = INITPLACEY;
	float separationplace = SEPARATIONPLACE;

	for (int i = 0; i < 8; i++) {
		lvl3Pos[i][0] = { initplaceX, initplaceY };
		lvl3Type[i][0] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 2; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 6; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 7; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_EMPLY, PLACE_PLAYER };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][1] = { initplaceX, initplaceY };
		lvl3Type[i][1] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 2; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 4; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 4; i < 7; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][2] = { initplaceX, initplaceY };
		lvl3Type[i][2] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 3; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 5; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 7; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][3] = { initplaceX, initplaceY };
		lvl3Type[i][3] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 3; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 5; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 6; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_EMPLY, PLACE_BOX};
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 7; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][4] = { initplaceX, initplaceY };
		lvl3Type[i][4] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 2; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 3; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 6; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 7; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][5] = { initplaceX, initplaceY };
		lvl3Type[i][5] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 2; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 2; i < 3; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_EMPLY, PLACE_BOX };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 3; i < 6; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 7; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 7; i < 8; i++) {
		lvl3Pos[i][6] = { initplaceX, initplaceY };
		lvl3Type[i][6] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 1; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 1; i < 5; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_EMPLY, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 5; i < 6; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BUTTON, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	for (int i = 6; i < 8; i++) {
		lvl3Pos[i][7] = { initplaceX, initplaceY };
		lvl3Type[i][7] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
	initplaceX = INITPLACEX;
	initplaceY += SEPARATIONPLACE;
	for (int i = 0; i < 8; i++) {
		lvl3Pos[i][8] = { initplaceX, initplaceY };
		lvl3Type[i][8] = { PLACE_BRICK, PLACE_EMPLY };
		initplaceX += SEPARATIONPLACE;
	}
}

Vector2 DataManager::GetPosInlvl3(int _x, int _y) {
	return lvl3Pos[_x][_y];
}
Vector2 DataManager::GetTypeInlvl3(int _x, int _y) {
	return lvl3Type[_x][_y];
}
void DataManager::SetXPosTypelvl3(int _x, int _y, int _type) {
	lvl3Type[_x][_y].x = _type;
}
void DataManager::SetYPosTypelvl3(int _x, int _y, int _type) {
	lvl3Type[_x][_y].y = _type;
}