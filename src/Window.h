#ifndef WINDOW_H
#define WINDOW_H
#include"Global_Includes.h"

class Window {
private:
	const char* tittle;
	int screenWidth;
	int screenHeight;
	int fps;
public:
	Window();
	Window(const char* _tittle, int _screenWidth, int _screenHeight, int _fps);
	void setTittle(const char* _tittle);
	const char* getTittle();
	void setWidth(int _screenWidth);
	int getWidth();
	void setHeight(int _screenHeight);
	int getHeight();
	void setFps(int _fps);
	int getFps();
	//-----------
	void StartWindow();
	void setWindowFps();
	void WindowClose();
};

#endif // !WINDOW_H