#include"Window.h"

Window::Window() {
	setTittle("NANIMONAI");
	setWidth(0);
	setHeight(0);
	setFps(0);
}
Window::Window(const char* _tittle, int _screenWidth, int _screenHeight, int _fps) {
	setTittle(_tittle);
	setWidth(_screenWidth);
	setHeight(_screenHeight);
	setFps(_fps);
}
void Window::setTittle(const char* _tittle) {
	tittle = _tittle;
}
const char* Window::getTittle() {
	return tittle;
}
void Window::setWidth(int _screenWidth) {
	screenWidth = _screenWidth;
}
int Window::getWidth() {
	return screenWidth;
}
void Window::setHeight(int _screenHeight) {
	screenHeight = _screenHeight;
}
int Window::getHeight() {
	return screenHeight;
}
void Window::setFps(int _fps) {
	fps = _fps;
}
int Window::getFps() {
	return fps;
}
//----------
void Window::StartWindow() {
	InitWindow(getWidth(), getHeight(), getTittle());
}
void Window::setWindowFps() {
	SetTargetFPS(getFps());
}
void Window::WindowClose() {
	CloseWindow();
}