#ifndef PROCESS_H
#define PROCESS_H
#include"Global_Includes.h"

class Process {
private:

public:
	virtual void process(DataManager* data) = 0;
};

#endif // !PROCESS_H