#include"Player.h"

Player::Player() {
	input = new Input();
}
Player::~Player() {
	if (input != NULL) delete input;
}

//---

void Player::Move(DataManager* data, int _mapTamX, int _mapTamY) {
	bool activated = true;
	for (int i = 0; i < _mapTamX; i++) {
		for (int j = 0; j < _mapTamY; j++) {
			if (data->GetTypeInlvl3(i, j).y == PLACE_PLAYER) {
				//----
				if (input->ButtonPressed(KEY_W)) {
					if (data->GetTypeInlvl3(i, j - 1).x == PLACE_BUTTON && data->GetTypeInlvl3(i, j - 1).y != PLACE_BOX ||
						data->GetTypeInlvl3(i, j - 1).x == PLACE_EMPLY && data->GetTypeInlvl3(i ,j - 1).y != PLACE_BOX) {
						if (activated) {
							data->SetYPosTypelvl3(i, j, PLACE_EMPLY);
							data->SetYPosTypelvl3(i, j - 1, PLACE_PLAYER);
							if (!IsSoundPlaying(data->GetPasoSound())) {
								PlaySound(data->GetPasoSound());
							}

							activated = false;
						}
					}
					if (data->GetTypeInlvl3(i, j - 1).y == PLACE_BOX && data->GetTypeInlvl3(i, j - 2).x == PLACE_EMPLY && data->GetTypeInlvl3(i, j - 2).y == PLACE_EMPLY ||
						data->GetTypeInlvl3(i, j - 1).y == PLACE_BOX && data->GetTypeInlvl3(i, j - 2).x == PLACE_BUTTON && data->GetTypeInlvl3(i, j - 2).y == PLACE_EMPLY) {
						if (activated) {
							data->SetYPosTypelvl3(i, j, PLACE_EMPLY);
							data->SetYPosTypelvl3(i, j - 1, PLACE_PLAYER);
							data->SetYPosTypelvl3(i, j - 2, PLACE_BOX);
							if (!IsSoundPlaying(data->GetPasoSound())) {
								PlaySound(data->GetPasoSound());
							}

							activated = false;
						}
					}
				}
				//----
				if (input->ButtonPressed(KEY_S)) {
					if (data->GetTypeInlvl3(i, j + 1).x == PLACE_BUTTON && data->GetTypeInlvl3(i, j + 1).y != PLACE_BOX ||
						data->GetTypeInlvl3(i, j + 1).x == PLACE_EMPLY && data->GetTypeInlvl3(i, j + 1).y != PLACE_BOX) {
						if (activated) {
							data->SetYPosTypelvl3(i, j, PLACE_EMPLY);
							data->SetYPosTypelvl3(i, j + 1, PLACE_PLAYER);
							if (!IsSoundPlaying(data->GetPasoSound())) {
								PlaySound(data->GetPasoSound());
							}

							activated = false;
						}
					}
					if (data->GetTypeInlvl3(i, j + 1).y == PLACE_BOX && data->GetTypeInlvl3(i, j + 2).x == PLACE_EMPLY && data->GetTypeInlvl3(i, j + 2).y == PLACE_EMPLY ||
						data->GetTypeInlvl3(i, j + 1).y == PLACE_BOX && data->GetTypeInlvl3(i, j + 2).x == PLACE_BUTTON && data->GetTypeInlvl3(i, j + 2).y == PLACE_EMPLY) {
						if (activated) {
							data->SetYPosTypelvl3(i, j, PLACE_EMPLY);
							data->SetYPosTypelvl3(i, j + 1, PLACE_PLAYER);
							data->SetYPosTypelvl3(i, j + 2, PLACE_BOX);
							if (!IsSoundPlaying(data->GetPasoSound())) {
								PlaySound(data->GetPasoSound());
							}

							activated = false;
						}
					}
				}
				//----
				if (input->ButtonPressed(KEY_A)) {
					if (data->GetTypeInlvl3(i - 1, j).x == PLACE_BUTTON && data->GetTypeInlvl3(i - 1, j).y != PLACE_BOX ||
						data->GetTypeInlvl3(i - 1, j).x == PLACE_EMPLY && data->GetTypeInlvl3(i - 1, j).y != PLACE_BOX) {
						if (activated) {
							data->SetYPosTypelvl3(i, j, PLACE_EMPLY);
							data->SetYPosTypelvl3(i - 1, j, PLACE_PLAYER);
							if (!IsSoundPlaying(data->GetPasoSound())) {
								PlaySound(data->GetPasoSound());
							}

							activated = false;
						}
					}
					if (data->GetTypeInlvl3(i - 1, j).y == PLACE_BOX && data->GetTypeInlvl3(i - 2, j).x == PLACE_EMPLY && data->GetTypeInlvl3(i - 2, j).y == PLACE_EMPLY ||
						data->GetTypeInlvl3(i - 1, j).y == PLACE_BOX && data->GetTypeInlvl3(i - 2, j).x == PLACE_BUTTON && data->GetTypeInlvl3(i - 2, j).y == PLACE_EMPLY) {
						if (activated) {
							data->SetYPosTypelvl3(i, j, PLACE_EMPLY);
							data->SetYPosTypelvl3(i - 1, j, PLACE_PLAYER);
							data->SetYPosTypelvl3(i - 2, j, PLACE_BOX);
							if (!IsSoundPlaying(data->GetPasoSound())) {
								PlaySound(data->GetPasoSound());
							}

							activated = false;
						}
					}
				}
				//----
				if (input->ButtonPressed(KEY_D)) {
					if (data->GetTypeInlvl3(i + 1, j).x == PLACE_BUTTON && data->GetTypeInlvl3(i + 1, j).y != PLACE_BOX ||
						data->GetTypeInlvl3(i + 1, j).x == PLACE_EMPLY && data->GetTypeInlvl3(i + 1, j).y != PLACE_BOX) {
						if (activated) {
							data->SetYPosTypelvl3(i, j, PLACE_EMPLY);
							data->SetYPosTypelvl3(i + 1, j, PLACE_PLAYER);
							if (!IsSoundPlaying(data->GetPasoSound())) {
								PlaySound(data->GetPasoSound());
							}

							activated = false;
						}
					}
					if (data->GetTypeInlvl3(i + 1, j).y == PLACE_BOX && data->GetTypeInlvl3(i + 2, j).x == PLACE_EMPLY && data->GetTypeInlvl3(i + 2, j).y == PLACE_EMPLY ||
						data->GetTypeInlvl3(i + 1, j).y == PLACE_BOX && data->GetTypeInlvl3(i + 2, j).x == PLACE_BUTTON && data->GetTypeInlvl3(i + 2, j).y == PLACE_EMPLY) {
						if (activated) {
							data->SetYPosTypelvl3(i, j, PLACE_EMPLY);
							data->SetYPosTypelvl3(i + 1, j, PLACE_PLAYER);
							data->SetYPosTypelvl3(i + 2, j, PLACE_BOX);
							if (!IsSoundPlaying(data->GetPasoSound())) {
								PlaySound(data->GetPasoSound());
							}

							activated = false;
						}
					}
				}
				//----
			}
		}
	}
}