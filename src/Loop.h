#ifndef LOOP_H
#define LOOP_H
#include"Logic.h"
#include"Draw.h"

#define TAM 2

class Loop {
private:
	Process* process[TAM];
public:
	Loop();
	~Loop();
	void StartLoop(DataManager* data);
};

#endif // !LOOP_H