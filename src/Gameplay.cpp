#include"Gameplay.h"

Gameplay::Gameplay() {
	input = new Input();
	player = new Player();
}
Gameplay::~Gameplay() {
	if (input != NULL) delete input;
	if (player != NULL) delete player;
}

//---

void Gameplay::LogicSwitcher(DataManager* data) {

	switch (data->GetInGameInstance()) {
	case 0:
		LogicSceneTutorial(data);
		break;
	case 1:
		LogicGame(data);
		break;
	}

}

void Gameplay::LogicSceneTutorial(DataManager* data) {

	data->PlusDelta(GetFrameTime());
	data->SetLoadBar(100, POSBARY, (data->GetDelta() * 1080.0f) / 3.0f, 5);

	if (data->GetDelta() >= 3.0f) {

		data->Generatelvl1();
		data->SetInGameInstance(1);
		data->SetDelta(0.0f);

	}

}

void Gameplay::LogicGame(DataManager* data) {

	if (data->GetGameActivate()) {
		PlaySound(data->GetGameSound());
		data->SetGameActivate(false);
	}
	if (!IsSoundPlaying(data->GetGameSound())) {
		data->SetGameActivate(false);
	}

	data->SetCheckPauseButton(CheckCollisionPointRec(GetMousePosition(), data->GetPauseButton()));
	data->SetCheckRestartButton(CheckCollisionPointRec(GetMousePosition(), data->GetRestartButton()));
	data->SetCheckLeaveButton(CheckCollisionPointRec(GetMousePosition(), data->GetLeaveButton()));

	if (data->GetCheckPauseButton()) {

		if (input->MouseClickPressed(MOUSE_LEFT_BUTTON)) {
			data->SwitchPause();
		}

	}

	if (data->GetPause()) {

		if (IsSoundPlaying(data->GetGameSound())) {
			PauseSound(data->GetGameSound());
		}
		
		if (data->GetCheckRestartButton()) {

			if (input->MouseClickPressed(MOUSE_LEFT_BUTTON)) {

				switch (data->GetGamelvl())	{
				case 0:
					data->Generatelvl1();
					break;
				case 1:
					data->Generatelvl2();
					break;
				case 2:
					data->Generatelvl3();
					break;
				case 3:
					data->Generatelvl4();
					break;
				case 4:
					data->Generatelvl5();
					break;
				}

			}

		}

		if (data->GetCheckLeaveButton()) {
			if (input->MouseClickPressed(MOUSE_LEFT_BUTTON)) {
				data->SetGameInstance(0);
				data->SetInGameInstance(0);
				data->SetGamelvl(0);
				data->SwitchPause();
			}
		}
		
	}
	else {

		if (!IsSoundPlaying(data->GetGameSound())) {
			ResumeSound(data->GetGameSound());
		}

		player->Move(data, 8, 9);
		
		switch (data->GetGamelvl()) {
		case 0:
			//---
			data->CheckWinlvl(8, 9, 2);

			if (data->GetWinlvl()) {
				data->SetGamelvl(1);
				data->SwitchWinlvl();
				data->Generatelvl2();
			}
			//---
			break;
		case 1:
			//---
			data->CheckWinlvl(8, 9, 4);

			if (data->GetWinlvl()) {
				data->SetGamelvl(2);
				data->SwitchWinlvl();
				data->Generatelvl3();
			}
			//---
			break;
		case 2:
			//---
			data->CheckWinlvl(8, 9, 4);

			if (data->GetWinlvl()) {
				data->SetGamelvl(3);
				data->SwitchWinlvl();
				data->Generatelvl4();
			}
			//---
			break;
		case 3:
			//---
			data->CheckWinlvl(8, 9, 7);

			if (data->GetWinlvl()) {
				data->SetGamelvl(4);
				data->SwitchWinlvl();
				data->Generatelvl5();
			}
			//---
			break;
		case 4:
			//---
			data->CheckWinlvl(8, 9, 5);

			if (data->GetWinlvl()) {
				data->SetGamelvl(0);
				data->SwitchWinlvl();
				data->SetInGameInstance(0);
				data->SetGameInstance(0);
			}
			//---
			break;
		}

	}
}

//---

void Gameplay::DrawSwitcher(DataManager* data) {

	switch (data->GetInGameInstance()) {
	case 0:
		DrawSceneTutorial(data);
		break;
	case 1:
		DrawGame(data);
		break;
	}

}

void Gameplay::DrawSceneTutorial(DataManager* data) {

	DrawRectangleRec(data->GetLoadBar(), WHITE);

}

void Gameplay::DrawGame(DataManager* data) {

	for (int i = 0; i < 8; i++) {

		for (int q = 0; q < 9; q++) {

			if (data->GetTypeInlvl3(i, q).x == PLACE_EMPLY) {

				DrawTextureEx(data->GetFlorTr(), data->GetPosInlvl3(i, q), 0.0f, 1.0f, WHITE);

			}
			if (data->GetTypeInlvl3(i, q).x == PLACE_BRICK) {

				if (data->GetTypeInlvl3(i, q + 1).x == PLACE_BRICK) {

					DrawTextureEx(data->GetBricksTopTr(), data->GetPosInlvl3(i, q), 0.0f, 1.0f, WHITE);

				}
				else
					if (data->GetTypeInlvl3(i, q + 1).x == PLACE_EMPLY || data->GetTypeInlvl3(i, q + 1).x == PLACE_BUTTON) {

						DrawTextureEx(data->GetBricksDownTr(), data->GetPosInlvl3(i, q), 0.0f, 1.0f, WHITE);

					}
			}
			if (data->GetTypeInlvl3(i, q).x == PLACE_BUTTON) {

				DrawTextureEx(data->GetPressurePlateTr(), data->GetPosInlvl3(i, q), 0.0f, 1.0f, WHITE);

			}
			if (data->GetTypeInlvl3(i, q).y == PLACE_BOX) {

				DrawTextureEx(data->GetBoxTr(), data->GetPosInlvl3(i, q), 0.0f, 1.0f, WHITE);

			}
			if (data->GetTypeInlvl3(i, q).y == PLACE_PLAYER) {

				DrawTextureEx(data->GetPlayerTr(), data->GetPosInlvl3(i, q), 0.0f, 0.3f, WHITE);

			}
		}
	}

	if (!data->GetCheckPauseButton())
		DrawTextureEx(data->GetPauseButtonTr(), data->GetPauseButtonPos(), 0.0f, 4.0f, WHITE);
	else 
		DrawTextureEx(data->GetPauseButtonTr(), data->GetPauseButtonPos(), 0.0f, 4.0f, SKYBLUE);

	if (data->GetPause()) {
		if (!data->GetCheckRestartButton()) {
			DrawRectangleRec(data->GetRestartButton(), WHITE);
			DrawText("RESTART", 1005, 340, 25, BLACK);
		}
		if (data->GetCheckRestartButton()) {
			DrawRectangleRec(data->GetRestartButton(), SKYBLUE);
			DrawText("RESTART", 1005, 340, 25, BLACK);
		}

		if (!data->GetCheckLeaveButton()) {
			DrawRectangleRec(data->GetLeaveButton(), WHITE);
			DrawText("LEAVE", 1005, 540, 25, BLACK);
		} 
		if (data->GetCheckLeaveButton()) {
			DrawRectangleRec(data->GetLeaveButton(), SKYBLUE);
			DrawText("LEAVE", 1005, 540, 25, BLACK);
		}
	}
}