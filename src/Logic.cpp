#include"Logic.h"

Logic::Logic() {
	menu = new Menu();
	gameplay = new Gameplay();
}
Logic::~Logic() {
	if (menu != NULL) delete menu;
	if (gameplay != NULL) delete gameplay;
}

//---

void Logic::process(DataManager* data) {
	switch (data->GetGameInstance()) {
	case 0:
		menu->LogicSwitcher(data);
		break;
	case 1:
		gameplay->LogicSwitcher(data);
		break;
	}
}