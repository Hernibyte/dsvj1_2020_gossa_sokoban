#ifndef DATAMANAGER_H
#define DATAMANAGER_H
#include"Global_Includes.h"

#define PLACE_EMPLY 0
#define PLACE_BRICK 1
#define PLACE_BOX 2
#define PLACE_BUTTON 3
#define PLACE_PLAYER 4

#define INITPLACEX 400
#define INITPLACEY 200
#define SEPARATIONPLACE 64
#define TAMPLACE 64

#define POSBARY 950

class DataManager {
private:
	bool closeGame; //Se usa para cerrar el juego de manera secundaria
	float delta; //Timer local
	bool pauseGame = false; //pause
	//--- Textures
	Texture2D PlayButtonTr;
	Texture2D closeButtonTr;
	Texture2D pauseButtonTr;
	Texture2D brickTopTr;
	Texture2D brickDownTr;
	Texture2D floorTr;
	Texture2D pressurePlateTr;
	Texture2D boxTr;
	Texture2D playerTr;
	//--- Sounds
	Sound MenuSoundTrack;
	bool MenuSoundActivate = true;

	Sound GameSound;
	bool GameSoundActivate;

	Sound PasoSound;
	//bool PasoActivate;
	//---
	int gameInstance; //Determina en donde se encuentra el jugador (menu, juego, etc)
	int inMenuIntance; //Determina el estado del jugador dentro de la intancia menu
	int inGameInstance; //Determina el estado del jugador dentro de la instancia gameplay
	int gamelvl; //Determina el level del jugador
	//---
	Rectangle playButton;
	Vector2 playButtonPos;
	bool checkPlayButton;
	//---
	Rectangle closeButton;
	Vector2 closeButtonPos;
	bool checkCloseButton;
	//---
	Vector2 optionButtonCenter;
	float optionButtonRadius;
	bool checkOptionButton;
	//---
	Rectangle pauseButton;
	Vector2 pauseButtonPos;
	bool checkPauseButton;
	//---
	Rectangle restartButton;
	Vector2 restartButtonPos;
	bool checkRestartButton;
	//---
	Rectangle leaveButton;
	Vector2 leaveButtonPos;
	bool checkLeaveButton;
	//---
	Rectangle loadBar;
	//---
	bool checkWinlvl;
	//---
	Vector2 lvl3Pos[8][9];
	Vector2 lvl3Type[8][9];
public:
	DataManager();
	~DataManager();
	//---
	void SetCloseGame(bool _closeGame);
	bool GetCloseGame();
	void SetDelta(float _delta);
	void PlusDelta(float _delta);
	float GetDelta();
	void SwitchPause();
	float GetPause();
	//--- Textures
	void SetPlayButtonTr(const char* filePath);
	Texture2D GetPlayButtonTr();
	void DeletePlayButtonTr();

	void SetCloseButtonTr(const char* filePath);
	Texture2D GetCloseButtonTr();
	void DeleteCloseButtonTr();

	void SetBricksTopTr(const char* filePath);
	Texture2D GetBricksTopTr();
	void DeleteBricksTopTr();

	void SetBricksDownTr(const char* filePath);
	Texture2D GetBricksDownTr();
	void DeleteBricksDownTr();

	void SetFloorTr(const char* filePath);
	Texture2D GetFlorTr();
	void DeleteFloorTr();

	void SetPressurePlateTr(const char* filePath);
	Texture2D GetPressurePlateTr();
	void DeletePressurePlateTr();

	void SetBoxTr(const char* filePath);
	Texture2D GetBoxTr();
	void DeleteBoxTr();
	//---
	void SetPlayerTr(const char* filePath);
	Texture2D GetPlayerTr();
	void DeletePlayerTr();
	//--- Sounds
	void SetMenuSound(const char* fileName);
	Sound GetMenuSound();
	void DeleteMenuSound();
	void SwitchMenuSoundActivate();
	bool GetMenuSoundActivate();

	void SetGameSound(const char* fileName);
	Sound GetGameSound();
	void DeleteGameSound();
	void SetGameActivate(bool _gameActivate);
	bool GetGameActivate();

	void SetPasoSound(const char* fileName);
	Sound GetPasoSound();
	void DeletePasoSound();
	//---
	void SetGameInstance(int _gameInstance);
	int GetGameInstance();
	void SetInMenuInstance(int _inMenuInstance);
	int GetInMenuInstance();
	void SetInGameInstance(int _inGameInstance);
	int GetInGameInstance();
	void SetGamelvl(int _gamelvl);
	int GetGamelvl();
	//---
	void SetPlayButton(float _x, float _y, float _width, float _height);
	Rectangle GetPlayButtonPos();
	Vector2 GetPlayButtonPosition();
	void SetCheckPlayButton(bool _checkPlayButton);
	bool GetCheckPlayButton();
	//---
	void SetCloseButton(float _x, float _y, float _width, float _height);
	Rectangle GetCloseButton();
	Vector2 GetCloseButtonPos();
	void SetCheckCloseButton(bool _checkCloseButton);
	bool GetCheckCloseButton();
	//---
	void SetOptionButton(int _optionButtonCenterX, int _optionButtonCenterY, float _optionButtonRadius);
	Vector2 GetOptionButtonCenter();
	float GetOptionButtonRadius();
	void SetCheckOptionButton(bool _checkOptionButton);
	bool GetCheckOptionButton();
	//---
	void SetPauseButton(float _x, float _y, float _width, float _height);
	Rectangle GetPauseButton();
	Vector2 GetPauseButtonPos();
	void SetCheckPauseButton(bool _checkPauseButton);
	bool GetCheckPauseButton();
	void SetPauseButtonTr(const char* filePath);
	Texture2D GetPauseButtonTr();
	void DeletePauseButtonTr();
	//---
	void SetRestartButton(float _x, float _y, float _width, float _height);
	Rectangle GetRestartButton();
	Vector2 GetRestartButtonPos();
	void SetCheckRestartButton(bool _checkRestartButton);
	bool GetCheckRestartButton();
	//---
	void SetLeaveButton(float _x, float _y, float _width, float _height);
	Rectangle GetLeaveButton();
	Vector2 GetLeaveButtonPos();
	void SetCheckLeaveButton(bool _checkLeaveButton);
	bool GetCheckLeaveButton();
	//---
	void SetLoadBar(float _x, float _y, float _width, float _height);
	Rectangle GetLoadBar();
	//---
	void SetWinlvl(bool _checkWinlvl);
	void SwitchWinlvl();
	bool GetWinlvl();
	void CheckWinlvl(int _mapTamX, int _mapTamY, int _CantButtons);
	//---
	void Generatelvl1();
	void Generatelvl2();
	void Generatelvl3();
	void Generatelvl4();
	void Generatelvl5();

	Vector2 GetPosInlvl3(int _x, int _y);
	Vector2 GetTypeInlvl3(int _x, int _y);
	void SetXPosTypelvl3(int _x, int _y, int _type);
	void SetYPosTypelvl3(int _x, int _y, int _type);
};

#endif // !DATAMANAGER_H