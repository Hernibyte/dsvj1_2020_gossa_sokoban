#ifndef GAMEPLAY_H
#define GAMEPLAY_H
#include"Input.h"
#include"Player.h"


class Gameplay {
private:
	Player* player;
	Input* input;
public:
	Gameplay();
	~Gameplay();
	//---
	void LogicSwitcher(DataManager* data);
	void LogicSceneTutorial(DataManager* data);
	void LogicGame(DataManager* data);
	//---
	void DrawSwitcher(DataManager* data);
	void DrawSceneTutorial(DataManager* data);
	void DrawGame(DataManager* data);
};

#endif // !GAMEPLAY_H