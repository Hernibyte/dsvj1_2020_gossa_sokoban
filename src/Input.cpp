#include"Input.h"

bool Input::MouseClickPressed(int type) {
	switch (type) {
	case MOUSE_LEFT_BUTTON:
		return IsMouseButtonPressed(MOUSE_LEFT_BUTTON);
		break;
	case MOUSE_RIGHT_BUTTON:
		return IsMouseButtonPressed(MOUSE_RIGHT_BUTTON);
		break;
	}
}
bool Input::MouseClickDown(int type) {
	switch (type) {
	case MOUSE_LEFT_BUTTON:
		return IsMouseButtonDown(MOUSE_LEFT_BUTTON);
		break;
	case MOUSE_RIGHT_BUTTON:
		return IsMouseButtonDown(MOUSE_RIGHT_BUTTON);
		break;
	}
}
bool Input::MouseClickUp(int type) {
	switch (type) {
	case MOUSE_LEFT_BUTTON:
		return IsMouseButtonUp(MOUSE_LEFT_BUTTON);
		break;
	case MOUSE_RIGHT_BUTTON:
		return IsMouseButtonUp(MOUSE_RIGHT_BUTTON);
		break;
	}
}
bool Input::MouseClickReleased(int type) {
	switch (type) {
	case MOUSE_LEFT_BUTTON:
		return IsMouseButtonReleased(MOUSE_LEFT_BUTTON);
		break;
	case MOUSE_RIGHT_BUTTON:
		return IsMouseButtonReleased(MOUSE_RIGHT_BUTTON);
		break;
	}
}

//---


bool Input::ButtonPressed(int type) {
	switch (type) {
	case KEY_W:
		return IsKeyPressed(KEY_W);
		break;
	case KEY_A:
		return IsKeyPressed(KEY_A);
		break;
	case KEY_S:
		return IsKeyPressed(KEY_S);
		break;
	case KEY_D:
		return IsKeyPressed(KEY_D);
		break;
	case KEY_E:
		return IsKeyPressed(KEY_E);
		break;
	}
}
bool Input::ButtonDown(int type) {
	switch (type) {
	case KEY_W:
		return IsKeyDown(KEY_W);
		break;
	case KEY_A:
		return IsKeyDown(KEY_A);
		break;
	case KEY_S:
		return IsKeyDown(KEY_S);
		break;
	case KEY_D:
		return IsKeyDown(KEY_D);
		break;
	case KEY_E:
		return IsKeyDown(KEY_E);
		break;
	}
}
bool Input::ButtonUp(int type) {
	switch (type) {
	case KEY_W:
		return IsKeyUp(KEY_W);
		break;
	case KEY_A:
		return IsKeyUp(KEY_A);
		break;
	case KEY_S:
		return IsKeyUp(KEY_S);
		break;
	case KEY_D:
		return IsKeyUp(KEY_D);
		break;
	case KEY_E:
		return IsKeyUp(KEY_E);
		break;
	}
}
bool Input::ButtonReleased(int type) {
	switch (type) {
	case KEY_W:
		return IsKeyReleased(KEY_W);
		break;
	case KEY_A:
		return IsKeyReleased(KEY_A);
		break;
	case KEY_S:
		return IsKeyReleased(KEY_S);
		break;
	case KEY_D:
		return IsKeyReleased(KEY_D);
		break;
	case KEY_E:
		return IsKeyReleased(KEY_E);
		break;
	}
}