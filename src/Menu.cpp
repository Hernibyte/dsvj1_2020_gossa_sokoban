#include"Menu.h"

Menu::Menu() {
	input = new Input();
}
Menu::~Menu() {
	if (input != NULL) delete input;
}

//---

void Menu::LogicSwitcher(DataManager* data) {
	if (data->GetMenuSoundActivate()) {
		PlaySound(data->GetMenuSound());
		data->SwitchMenuSoundActivate();
	}

	switch (data->GetInMenuInstance()) {
	case 0:
		PrincipalMenuInstance(data);
		break;
	case 1:
		OptionMenuInstance(data);
		break;
	case 2:
		SoundMenuInstance(data);
		break;
	}
}
void Menu::PrincipalMenuInstance(DataManager* data) {
	data->SetCheckPlayButton(CheckCollisionPointRec(GetMousePosition(), data->GetPlayButtonPos()));
	data->SetCheckCloseButton(CheckCollisionPointRec(GetMousePosition(), data->GetCloseButton()));
	//data->SetCheckOptionButton(CheckCollisionPointCircle(GetMousePosition(), data->GetOptionButtonCenter(), data->GetOptionButtonRadius()));
	
	if (data->GetCheckPlayButton()) {
		if (input->MouseClickPressed(MOUSE_LEFT_BUTTON)) {
			data->SetGameInstance(1);
			data->SetInGameInstance(0);
			StopSound(data->GetMenuSound());
		}
	}
	if (data->GetCheckCloseButton()) {
		if (input->MouseClickPressed(MOUSE_LEFT_BUTTON)) {
			data->SetCloseGame(true);
		}
	}
	/*
	if (data->GetCheckOptionButton()) {
		if (input->MouseClickPressed(MOUSE_LEFT_BUTTON)) {
			data->SetInMenuInstance(1);
			data->SetCheckOptionButton(false);
		}
	}*/
}
void Menu::OptionMenuInstance(DataManager* data) {
	data->SetCheckOptionButton(CheckCollisionPointCircle(GetMousePosition(), data->GetOptionButtonCenter(), data->GetOptionButtonRadius()));

	if (data->GetCheckOptionButton()) {
		if (input->MouseClickPressed(MOUSE_LEFT_BUTTON)) {
			data->SetInMenuInstance(0);
		}
	}
}
void Menu::SoundMenuInstance(DataManager* data) {

}

//---

void Menu::DrawSwitcher(DataManager* data) {
	switch (data->GetInMenuInstance()) {
	case 0:
		DrawPrincMenuInst(data);
		break;
	case 1:
		DrawOptMenuInst(data);
		break;
	case 2:
		DrawSoundMenuInst(data);
		break;
	}
}
void Menu::DrawPrincMenuInst(DataManager* data) {
	if (!data->GetCheckPlayButton())
		DrawTextureEx(data->GetPlayButtonTr(), data->GetPlayButtonPosition(), 0.0f, 8.0f, WHITE);
	else 
		if (data->GetCheckPlayButton())
			DrawTextureEx(data->GetPlayButtonTr(), data->GetPlayButtonPosition(), 0.0f, 8.0f, SKYBLUE);

	if (!data->GetCheckCloseButton())
		DrawTextureEx(data->GetCloseButtonTr(), data->GetCloseButtonPos(), 0.0f, 4.0f, WHITE);
	else
		if (data->GetCheckCloseButton())
			DrawTextureEx(data->GetCloseButtonTr(), data->GetCloseButtonPos(), 0.0f, 4.0f, RED);
	/*
	if (!data->GetCheckOptionButton())
		DrawCircleV(data->GetOptionButtonCenter(), data->GetOptionButtonRadius(), RED);
	else
		if (data->GetCheckOptionButton())
			DrawCircleV(data->GetOptionButtonCenter(), data->GetOptionButtonRadius(), BLUE);
			*/
}
void Menu::DrawOptMenuInst(DataManager* data) {
	/*
	DrawRectangle(15, 700, 50, 240, RED);

	DrawPrincMenuInst(data); // fondo del menu principal
	*/
}
void Menu::DrawSoundMenuInst(DataManager* data) {

}