#ifndef LOGIC_H
#define LOGIC_H
#include"Process.h"
#include"Menu.h"
#include"Gameplay.h"

class Logic : public Process {
private:
	Menu* menu;
	Gameplay* gameplay;
public:
	Logic();
	~Logic();
	//---
	void process(DataManager* data);
};

#endif // !LOGIC_H